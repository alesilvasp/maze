const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     WM    W WOW",
    "W W W WWW WWWWW W W W",
    "W WLW  BW    CW W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W WPW",
    "W     W W W   W W W W",
    "W WWWWW W WWWWW W W W",
    "W      TW       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

const maze = document.getElementById('largeContainer')
const buttonReset = document.getElementById('reset')
const spanWin = document.createElement('span');
const winDiv = document.getElementById('winMsg')
const burguerList = document.getElementById('burguerList')


let walls = '';
let wall = '';
let space = '';
let burguer = '';
let burguer2 = '';
let lettuce = '';
let picles = '';
let sauce = '';
let cheese = '';
let onion = '';
let start = '';
let finish = '';

for (let i = 0; i < map.length; i++) {
    let paredes = map[i];
    walls = document.createElement('div');
    walls.classList.add('line');
    maze.appendChild(walls)
    for (let j = 0; j < paredes.length; j++) {
        let cell = paredes[j]
        if (cell === ' ') {
            space = document.createElement('div')
            space.classList.add('space')
            walls.appendChild(space)
        }
        else if (cell === 'W') {
            wall = document.createElement('div')
            wall.classList.add('wall')
            walls.appendChild(wall)
        }
        else if (cell === 'S') {
            start = document.createElement('div')
            start.classList.add('start')
            walls.appendChild(start)
        } else if (cell === 'F') {
            finish = document.createElement('div')
            finish.classList.add('finish')
            walls.appendChild(finish)
        } else if (cell === 'B') {
            burguer = document.createElement('div')
            burguer.classList.add('burguer')
            walls.appendChild(burguer)
        } else if (cell === 'L') {
            lettuce = document.createElement('div')
            lettuce.classList.add('lettuce')
            walls.appendChild(lettuce)
        } else if (cell === 'P') {
            picles = document.createElement('div')
            picles.classList.add('picles')
            walls.appendChild(picles)
        } else if (cell === 'M') {
            sauce = document.createElement('div')
            sauce.classList.add('sauce')
            walls.appendChild(sauce)
        } else if (cell === 'C') {
            cheese = document.createElement('div')
            cheese.classList.add('cheese')
            walls.appendChild(cheese)
        } else if (cell === 'O') {
            onion = document.createElement('div')
            onion.classList.add('onion')
            walls.appendChild(onion)
        } else if (cell === 'T') {
            burguer2 = document.createElement('div')
            burguer2.classList.add('burguer2')
            walls.appendChild(burguer2)
        }
    }
}
const player = start;
let x = 9;
let y = 0;
let playerPosition = map[x][y]

let playerMoveUp = 270;
let playerMoveLeft = 0;

const moveUp = () => {
    if (map[x - 1][y] !== 'W' && map[x - 1][y] !== undefined) {
        playerMoveUp -= 30;
        player.style.top = playerMoveUp + 'px';
        x -= 1
        itensCount()
    }
}

const moveDown = () => {
    if (map[x + 1][y] !== 'W' && map[x + 1][y] !== undefined) {
        playerMoveUp += 30;
        player.style.top = playerMoveUp + 'px';
        x += 1
        itensCount()
    }
}

const moveLeft = () => {
    if (map[x][y - 1] !== "W" && map[x][y - 1] !== undefined) {
        playerMoveLeft -= 30;
        player.style.left = playerMoveLeft + 'px';
        y -= 1
        itensCount()
        map[x][y] = 'S'
    }
}

const moveRight = () => {
    if (map[x][y + 1] !== 'W' && map[x][y + 1] !== undefined) {
        playerMoveLeft += 30;
        player.style.left = playerMoveLeft + 'px';
        y += 1
        itensCount();
    }
    if (map[x][y] === 'F' && hasAllItens()) {
        winMsg()
    } else if (map[x][y] === 'F' && !hasAllItens()) {
        spanWin.innerText = 'Ooops! Go back and complete the "Bag Mic"!'
        winDiv.appendChild(spanWin)
    }
}


document.addEventListener('keydown', (evt) => {
    if (evt.key === 'ArrowDown') {
        buttonReset.innerText = 'Reset'
        moveDown();
    } else if (evt.key === 'ArrowUp') {
        buttonReset.innerText = 'Reset'
        moveUp();
    } else if (evt.key === 'ArrowLeft') {
        buttonReset.innerText = 'Reset'
        moveLeft();
    } else if (evt.key === 'ArrowRight') {
        buttonReset.innerText = 'Reset'
        moveRight();
    }
})

const hasAllItens = () => {
    let soma = itemCheese + itemLettuce + itemBurguer + itemOnion + itemPicles + itemSauce + itemBurguer2;
    if (soma >= 8) {
        return true
    }
}

const winMsg = () => {
    spanWin.classList.add('span_win');
    spanWin.innerText = ''
    spanWin.innerText = 'Hummmmmmm. You Win!!'
    winDiv.appendChild(spanWin)
}

const resetGame = () => {
    buttonReset.innerText = 'Start Game'
    x = 9;
    y = 0;
    itemLettuce = 0;
    itemBurguer = 0;
    itemBurguer2 = 0;
    itemCheese = 0;
    itemOnion = 0;
    itemSauce = 0;
    itemPicles = 0;
    lettuce.classList.add('lettuce')
    burguer.classList.add('burguer')
    burguer2.classList.add('burguer2')
    sauce.classList.add('sauce')
    onion.classList.add('onion')
    cheese.classList.add('cheese')
    picles.classList.add('picles')
    lettuceCount.innerText = `Lettuce: ${0} / 1`
    burguerCount.innerText = `Meat: ${itemBurguer} / 2`
    cheeseCount.innerText = `Cheese: ${itemCheese} / 1`
    onionCount.innerText = `Onion: ${itemOnion} / 1`
    sauceCount.innerText = `Sauce: ${itemSauce} / 1`
    piclesCount.innerText = `Picles: ${itemPicles} / 1`
    playerPosition = map[x][y]
    spanWin.innerText = '';
    playerMoveLeft = 0;
    playerMoveUp = 270;
    player.style.left = playerMoveLeft + 'px';
    player.style.top = playerMoveUp + 'px';
}

buttonReset.addEventListener('click', resetGame)

let itemLettuce = 0;
let itemBurguer = 0;
let itemBurguer2 = 0;
let itemCheese = 0;
let itemOnion = 0;
let itemSauce = 0;
let itemPicles = 0;

lettuceCount = document.querySelector('.item_list--lettuce')
burguerCount = document.querySelector('.item_list--meat')
cheeseCount = document.querySelector('.item_list--cheese')
onionCount = document.querySelector('.item_list--onion')
sauceCount = document.querySelector('.item_list--sauce')
piclesCount = document.querySelector('.item_list--picles')

const itensCount = () => {
    if (map[x][y] === 'L' && itemLettuce === 0) {
        itemLettuce += 1
        lettuceCount.innerText = `Lettuce: ${itemLettuce} / 1`
        let lettuce = document.querySelector('.lettuce')
        lettuce.classList.remove('lettuce')
        lettuce.classList.add('space')
    } else if (map[x][y] === 'B' && itemBurguer === 0) {
        itemBurguer = itemBurguer2 + 1
        burguerCount.innerText = `Meat: ${itemBurguer} / 2`
        let burguer1 = document.querySelector('.burguer')
        burguer1.classList.remove('burguer')
        burguer1.classList.add('space')
    } else if (map[x][y] === 'T' && itemBurguer2 === 0) {
        itemBurguer2 = itemBurguer + 1
        burguerCount.innerText = `Meat: ${itemBurguer2} / 2`
        let burguer2 = document.querySelector('.burguer2')
        burguer2.classList.remove('burguer2')
        burguer2.classList.add('space')
    } else if (map[x][y] === 'C' && itemCheese === 0) {
        itemCheese += 1
        cheeseCount.innerText = `Cheese: ${itemCheese} / 1`
        let cheese = document.querySelector('.cheese')
        cheese.classList.remove('cheese')
        cheese.classList.add('space')
    } else if (map[x][y] === 'O' && itemOnion === 0) {
        itemOnion += 1
        onionCount.innerText = `Onion: ${itemOnion} / 1`
        let onion = document.querySelector('.onion')
        onion.classList.remove('onion')
        onion.classList.add('space')
    } else if (map[x][y] === 'M' && itemSauce === 0) {
        itemSauce += 1
        sauceCount.innerText = `Sauce: ${itemSauce} / 1`
        let sauce = document.querySelector('.sauce')
        sauce.classList.remove('sauce')
        sauce.classList.add('space')
    } else if (map[x][y] === 'P' && itemPicles === 0) {
        itemPicles += 1
        piclesCount.innerText = `Picles: ${itemPicles} / 1`
        let picles = document.querySelector('.picles')
        picles.classList.remove('picles')
        picles.classList.add('space')
    }
}